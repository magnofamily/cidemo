"""
"""

app_location = "/home/fm/proj/cidemo/repos/calculator/device"
app_name = "basic.py"


from subprocess import Popen, PIPE
import pytest

def test_add(val1=3.4, val2=2.6, expected=6.0):
    proc = Popen(["./"+app_name, "add", str(val1), str(val2)], stdout=PIPE, stderr=PIPE, cwd=app_location)
    stdout, stderr = proc.communicate()
    returncode = proc.returncode

    assert returncode == 0
    assert stderr.strip() == ""
    assert stdout.strip() == str(expected)


def test_sub(val1=3.4, val2=2.6, expected=0.8):
    proc = Popen(["./"+app_name, "sub", str(val1), str(val2)], stdout=PIPE, stderr=PIPE, cwd=app_location)
    stdout, stderr = proc.communicate()
    returncode = proc.returncode

    assert returncode == 0
    assert stderr.strip() == ""
    assert stdout.strip() == str(expected)




if __name__ == "__main__":
    test_add()
