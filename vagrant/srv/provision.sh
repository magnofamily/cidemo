#!/bin/bash -xe

#export DEBIAN_FRONTEND=noninteractive

SECONDS=0
echo "$(date): Begin provisioning"

# Networking
sudo dhclient eth1  # make sure the iface is up/dhcp'ed

# Dependencies

## General
sudo apt-get update
sudo apt-get install -yq vim git

## NFS server
sudo apt-get install nfs-kernel-server nfs-common
sudo echo '/home/vagrant/share *(rw,sync,no_root_squash)' > /etc/exports
mkdir -p /home/vagrant/share
sudo service nfs-kernel-server restart

## Avahi
sudo apt-get install -yq avahi-daemon
sudo service avahi-daemon start || true


## Fetch cidemo
rm -rf cidemo
sudo -u vagrant git clone https://fmagmorg@bitbucket.org/magnofamily/cidemo.git


## Docker
sudo apt-get install -yq apt-transport-https ca-certificates
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list
echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" | sudo tee /etc/apt/sources.list.d/docker.list
sudo apt-get update
apt-cache policy docker-engine

### linux-image-extra-* kernel packages
sudo apt-get update
sudo apt-get install -yq linux-image-extra-$(uname -r) linux-image-extra-virtual
sudo apt-get install -yq docker-engine

sudo usermod -aG docker ubuntu  # requires reboot to apply
sudo usermod -aG docker vagrant  # requires reboot to apply
sudo service docker start || true

echo "start docker"

## Run services in docker containers

### volume-netshare (nfs)
wget -q https://github.com/ContainX/docker-volume-netshare/releases/download/v0.17/docker-volume-netshare_0.17_amd64.deb
sudo dpkg -i docker-volume-netshare_0.17_amd64.deb && rm docker-volume-netshare_0.17_amd64.deb
sudo service docker-volume-netshare start
sudo docker-volume-netshare nfs &
sleep 2

### lightweight webserver
docker pull fnichol/uhttpd
docker rm -f uhttpd_ci || true
docker run --name uhttpd_ci --restart=always -d -p 80:80 --volume-driver=nfs -v 127.0.0.1/home/vagrant/share/www:/www fnichol/uhttpd

### jenkins
#docker build -t jenkins:ci /home/vagrant/cidemo/docker/jenkins || true
#docker rm -f jenkins_ci || true
#docker run -d --name jenkins_ci --restart=always -p 8080:8080 -p 50000:50000 --env JAVA_OPTS="-Djenkins.install.runSetupWizard=false" --volume-driver=nfs -v 127.0.0.1/home/vagrant/share:/var/jenkins_home/share jenkins:ci

echo "$(date): Finish provisioning"
echo "Elapsed: $(($SECONDS / 60))m $(($SECONDS % 60))s"
